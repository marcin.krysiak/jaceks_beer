import { ServerRoute, RequestQuery } from 'hapi';

interface Request {
  query: RequestQuery;
}

export default {
  method: 'GET',
  path: `beers`,
  handler: {
    proxy: {
      mapUri: (request: Request) => {
        const queryString = request.query.food
          ? `?food=${request.query.food}`
          : '';
        return {
          uri: `https://api.punkapi.com/v2/beers${queryString}`,
        };
      },
    },
  },
} as ServerRoute;
