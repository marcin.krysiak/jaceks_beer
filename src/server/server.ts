import { Server, ServerRoute } from 'hapi';
import { config } from '../config';
import { routes } from './routes/';

const h2o2 = require('h2o2');
const good = require('good');
const goodConsole = require('good-console');

// Create a server with a host and port
const server: Server = new Server({
  host: process.env.HOST || config.HOST, // to allow external connections from Docker container
  port: process.env.PORT || config.PORT,
  routes: {
    cors: true,
  },
});

// Add API prefix
server.realm.modifiers.route.prefix = config.API_PATH;

// handle unhandledRejection
process.on('unhandledRejection', err => {
  console.error(err);
  process.exit(1);
});

// Start the server
const startServer = async function() {
  try {
    await server.register({ plugin: h2o2 });
    await server.register({
      plugin: good,
      options: {
        reporters: {
          console: [
            {
              module: goodConsole,
            },
            'stdout',
          ],
        },
      },
    });

    // Add all the routes within the routes folder
    routes.map((route: ServerRoute) => server.route(route));

    await server.start();

    console.info(`Server running at: ${server.info.uri}`);
  } catch (e) {
    console.log('Failed to load h2o2');
  }
};

startServer();
