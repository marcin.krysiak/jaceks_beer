import { RouterState } from 'react-router-redux';

export interface Beer {
  id: number;
  name: string;
  description: string;
  first_brewed: string;
}

export type Beers = Beer[] | string | null;

export interface State {
  beers: Beers;
  router: RouterState;
}

export const DEFAULT_STATE: State = {
  beers: null,
  router: {
    location: null,
  },
};
