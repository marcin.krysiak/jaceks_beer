export const BEERS_GET_LOADING = 'BEERS_GET_LOADING';
export const BEERS_GET_SUCCESS = 'BEERS_GET_SUCCESS';
export const BEERS_GET_ERROR = 'BEERS_GET_ERROR';

export {
  getBeersForFoodLoadingAction,
  getBeersForFoodSuccessAction,
  getBeersForFoodFailedAction,
} from './branches/getBeers';
