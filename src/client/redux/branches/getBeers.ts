import { createAction } from 'redux-actions';
import * as actionTypes from '../actions';
import { Beer, Beers, DEFAULT_STATE, State } from '../state';
import { Dispatch } from 'redux';
import { API_URL } from '../../index';

export interface ErrorResponse {
  message: string;
}

// Action creators
export const getBeersForFoodLoadingAction = createAction(
  actionTypes.BEERS_GET_LOADING,
);

export const getBeersForFoodSuccessAction = createAction(
  actionTypes.BEERS_GET_SUCCESS,
  (payload: Beer[]): Beer[] => payload,
);

export const getBeersForFoodFailedAction = createAction(
  actionTypes.BEERS_GET_ERROR,
  (payload: string): string => payload,
);

// Dispatcher
export const getBeersForFoodDispatcher = (food: string) => (
  dispatch: Dispatch<PayloadAction<Beers>>,
) => {
  dispatch(getBeersForFoodLoadingAction());

  const foodParam: string = food ? `?food=${food}` : '';
  return fetch(`${API_URL}beers${foodParam}`, {
    method: 'GET',
  })
    .then(response => response.json())
    .then(jsonResponse => dispatch(getBeersForFoodSuccessAction(jsonResponse)))
    .catch((error: ErrorResponse) =>
      dispatch(getBeersForFoodFailedAction(error.message)),
    );
};

// Reducer
export const beers = (
  state: State = DEFAULT_STATE,
  action: PayloadAction<Beers>,
) => {
  switch (action.type) {
    case actionTypes.BEERS_GET_LOADING:
      return 'loading';
    case actionTypes.BEERS_GET_SUCCESS:
      return action.payload;
    case actionTypes.BEERS_GET_ERROR:
      return action.payload;
    default:
      return null;
  }
};
