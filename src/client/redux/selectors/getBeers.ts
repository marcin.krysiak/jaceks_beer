import { State } from '../state';

export const beersSelector = (state: State) => state.beers;
