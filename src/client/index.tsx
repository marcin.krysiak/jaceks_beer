import 'isomorphic-fetch';
import './polyfills';
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import createBrowserHistory from 'history/createBrowserHistory';
import { Provider } from 'react-redux';
import { configureStore } from './store';
import { AppRouter } from './routes';
import { config as appConfig } from '../config';

export const API_URL =
  `${appConfig.PROTOCOL}://${appConfig.HOST}:${appConfig.PORT}${appConfig.API_PATH}`;
const store = configureStore();
const rootEl = document.getElementById('app');

export const history = createBrowserHistory();

if (!rootEl) console.error('missing "app" element');
else {
  // NOTE: This can be removed in React v17 as `render` will no longer try to `hydrate`.
  rootEl.innerHTML = '';
  ReactDOM.render(
    <Provider store={store}>
      <AppRouter />
    </Provider>,
    rootEl,
  );
}
