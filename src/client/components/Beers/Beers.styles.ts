import { prefix } from '../../utils/prefixer';
import { theme } from '../../global.styles';
import { CSSProperties } from '../../../../node_modules/@material-ui/core/styles/withStyles';
import createMuiTheme from '../../../../node_modules/@material-ui/core/styles/createMuiTheme';

export const styles = prefix({
  container: {
    display: 'flex',
    width: '100%',
    height: '100%',
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: theme.colors.polishRed,
    color: 'white',
  } as CSSProperties,
  theme: createMuiTheme({
    palette: {
      primary: {
        light: theme.colors.polishRed,
        main: theme.colors.polishRed,
        dark: theme.colors.polishRed,
        contrastText: theme.colors.polishRed,
      },
      text: {
        primary: theme.colors.darkGrey,
        secondary: theme.colors.polishRed,
      },
      background: {
        paper: theme.colors.white,
      },
    },
  }),
  paper: {
    margin: theme.gutter.large,
    padding: theme.gutter.large,
  } as CSSProperties,
  spacer: {
    margin: `${theme.gutter.large} 0`,
    borderRadius: 4,
  },
  redBorder: {
    border: `1px solid ${theme.colors.polishRed}`,
  },
  button: {
    margin: `0 0 0 ${theme.gutter.normal}`,
  },
  icon: {
    marginLeft: theme.gutter.small,
  },
  list: {
    overflow: 'auto',
    maxHeight: 300,
  },
});
