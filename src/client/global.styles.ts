export const theme = {
  colors: {
    white: '#ffffff',
    grey: '#aaa',
    darkGrey: '#333',
    polishRed: '#DC143C',
  },
  gutter: {
    small: '0.5rem',
    normal: '1rem',
    large: '2rem',
  },
};
