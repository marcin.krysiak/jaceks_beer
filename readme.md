# React, Redux, Hapi, Typescript Boilerplate

## File structure

- example-repos - themes, boilerplates etc to take an example from
- src
  - client - UI react - redux app
    - assets - images, fonts and other static assets
    - components - react components and containers
    - redux - redux layer files
      - branches - actions, dispatchers, reducers organized by functionality
      - selectors - selectors
      - actions.ts
    - utils - shared utils
    - global.styles.ts - shared css styles
    - index.html - mail html entry point
    - index.tsx - main react app file
    - routes.tsx - router config file
    - store.ts - redux store config file
  - server - _hapi.js backend_
    - controllers - controllers
    - routes - api routes
    - validate - api validators
    - server.ts - main server app file
  - config.ts - app config file
  - types.d.ts - global types and overrides
- .editorconfig - editor rules
- .gitattributes - git attributes
- .gitignore - ignored git files
- package.json - npm packege file
- readme.md - this file
- setupTests.ts - setup unit tests file
- tsconfig.json - typescript transpiler configuration file
- tslint.json - tsling linting roules and config
- webpack.config.js - webpack config file

## npm scripts

- npm start - start backend and frontend app
- npm t - unit test app and coverage report
- npm run test:client - test client only
- npm run test:server - test server only
- npm run precommit - lint and fix errors and run tests
- npm run client:build - build js bundle for the client app
- git:clean - remove all local branches except master
- check-updates - check for package updates
- precommit - precommig git hook
